package main 

import (
	"fmt"
	"os"
	"encoding/json"

	"./lib/fetch"
)

// needed to make json parse simpler
type AuResp struct {
	TrackSettings TrackSettings `json:"track_settings"`
}
type TrackSettings struct {
	DownloadLink string `json:"download_link_override"`
}

func main() {
	if len(os.Args) < 2 {
		fmt.Println("error: you must specify a valid track id")
		os.Exit(0)
	}
	fmt.Println("attempting to get info on track", os.Args[1])

	res, err := fetch.Request("get", "https://theartistunion.com/api/v3/tracks/"+os.Args[1]+".json", make(map[string]string))
	if err != nil {
		panic(err)
	}
	fmt.Println("got response, parsing...")

	var resp AuResp
	err = json.Unmarshal(res, &resp)
	if err != nil {
		panic(err)
	}

	fmt.Println(resp.TrackSettings.DownloadLink)
}